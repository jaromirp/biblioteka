package pl.sda.bib;

public class Ksiazka extends  Egzemplarz {

    private String tytul;


    public Ksiazka(String tytul, Autor[] autorzy, int rokWydania, int iloscStron) {
        super (autorzy, rokWydania, iloscStron);
        this.tytul = tytul;
    }

    @Override
    public String pobierzTytul() {
        return tytul;
    }
}

