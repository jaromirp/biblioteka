package pl.sda.bib;

public class Czlowiek {
    protected String imie, nazwisko;

    public Czlowiek(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    @Override
    public String toString() {
        return String.format("%s %s", imie, nazwisko);
    }
}

